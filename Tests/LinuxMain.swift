import XCTest

import APIKitTests

var tests = [XCTestCaseEntry]()
tests += APIKitTests.__allTests()

XCTMain(tests)
